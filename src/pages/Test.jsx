const Test = async () => {
    const url = 'https://shazam.p.rapidapi.com/charts/track?locale=en-US&pageSize=20&startFrom=0';
    const options = {
        method: 'GET',
        headers: {
            'X-RapidAPI-Key': '8a2b11df00msh69c8c0e0c12e3f5p160ce2jsnf57c9149cb96',
            'X-RapidAPI-Host': 'shazam.p.rapidapi.com'
        }
    };

    try {
        const response = await fetch(url, options);
        const result = await response.text();
        console.log(result);
    } catch (error) {
        console.error(error);
    }

    return <div>Test</div>;
}

export default Test;