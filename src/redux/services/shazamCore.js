import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const shazamCoreApi = createApi({
    reducerPath: 'shazamCoreApi',
    baseQuery: fetchBaseQuery({
        baseUrl: 'https://shazam.p.rapidapi.com​',
        prepareHeaderes: (headers) => {
            headers.set('X-RapidAPI-Key', '8a2b11df00msh69c8c0e0c12e3f5p160ce2jsnf57c9149cb96');
            headers.set('X-RapidAPI-Host', 'shazam.p.rapidapi.com');
            return headers;
        },
    }),
    endpoints: (builder) => ({
        getTopCharts: builder.query({ query: () => '/charts/track' }),
    })
});

export const {
    useGetTopChartsQuery,
} = shazamCoreApi;